<div align="center">

# Master's Thesis in Physics

by the title

### Epidemics on Generated and Real-World Networks

—

#### Modeling Immune Escape by a Random Walk in Antigenic Shape Space

---
<br>
<br>
<br>

conducted by [**Paul Wiesemeyer**](http://ts.iup.uni-heidelberg.de/people/paul-wiesemeyer/) at **University of Heidelberg's** [**Institute of Environmental Physics**](https://www.iup.uni-heidelberg.de/en)

supervised by [**Prof. Kurt Roth**](https://www.physik.uni-heidelberg.de/personen/lsf.php?details=8268) as part of the research group on

[Chaotic, Complex, and Evolving Environmental Systems](http://ts.iup.uni-heidelberg.de/research/ccees/)

in June 2021.

<br>

The thesis' computational model and simulation were built upon the research group's modeling framework

**``utopia``** (see [utopia-project.org](https://utopia-project.org/) and its [GitLab repository](https://gitlab.com/utopia-project)).

</div>
