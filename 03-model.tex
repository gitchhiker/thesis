% !TEX root = 0-THESIS.tex


\chapter{The \texttt{EpidNet} model} \label{chp:mod}
%


\gray{
\hfill\minipage[t]{\dimexpr0.7\linewidth-2\fboxsep-2\fboxrule\relax}
  \large
  \lettrine{>>M}{an mu\ss\ nicht} alles f\"ur wahr halten, man mu\ss\ es nur f\"ur notwendig halten.\\[1em]
%
  \hfill\small--- The prison chaplain, in Franz Kafka's \textit{Proce\ss}
\endminipage
}

In this chapter, we want to build up a computer model resting on the basic concepts of epidemic spreading, network analysis, and viral evolution that we introduced in the last chapter. The model shall allow for \1 tailoring to the epidemic situation, \2 numerical simulation, and \3 the (partly automated) evaluation of the results. To this end, it is built upon of the \textit{utopia framework} \citep{riedelHerdeanu+20, sevinchanHerdeanu+20} and makes use of the \texttt{dantro} package \citep{sevinchanHerdeanu+20a}.

The model will be exploratory in the sense that it does not have the purpose to test a specific hypothesis, and that it will allow for tinkering with a wide array of parameters. Because the model is of a multifaceted structure, we will first give an overview in words, followed by a class diagram. Then, we will build up the core structures, and in the end make some in-detail statements about the assumptions that this model underlies, as well as provide an overview of more general storylines that this model can be suited for.

As a setting, put yourself in the position of a researcher, or policymaker, who is faced with the onset of an epidemic and needs to know quantitatively how the situation unfolds. Say, that they (the researcher) seeks to model a new influenza strain or what is called an \gls{ili}, and wants to rapidly assess the dynamics by only providing a few numbers or making a sensitivity analysis in the respective parameter.

The contact structure in the population looked at shall ideally follow some organizational principle, that can be taken static in the time window of interest. Well-mixed systems can also be looked at---as a complete network---but the easier ODE approach can be taken instead. For complete random encounters (particles in a box-like) that may apply to some species on lower levels of complexity, an agent-based model on a manifold will be more suitable.

The size of the system is not restricted. Only due to computational limitations, for the scope of this thesis we will look at $N$ in the order of $10^4$. As a storyline, this could model a school, a conference, a cruise ship, or---as an system involving larger time scales while retaining perfect isolation---why not a voyage through outer space with only a fraction of humanity.

When it comes to the host individual, the situation must be such that its properties can be taken to be completely homogeneous. Variations in behavior or pre-existing health conditions must be negligible, such that an individual has only two defining properties: its current health state, and its contact pattern. The health state will consist of the \textit{compartment}, plus---in the case of evolving models---the immune history of positions in antigenic space. The contact pattern can be characterized by the number of direct contacts, but also includes the overall position in the network.

Coming back to the pathogen, the model allows for evolution in antigenic space, and is therefore suited for a scenario with very high evolutionary rates and a resulting quick immune escape. For moderate pathogen evolution, the static network assumption must remain intact, which could for example be the case for a wearing-on lockdown that suppresses random contacts over durations coming into the reach of evolutionary time scales.

The model starts with a formulation of the microscopic processes---at the scale of a single host and its vicinity. It then employs the graph as an exact description of the contact structure at \textit{all} scales to infer a macroscopic description of the epidemiological quantities of interest. It is therefore a \textit{bottom-up} model that uses the contact network as a ``ladder'' to climb the scales. The macroscopic quantities obtained are then for instance the mean state over \textit{all} nodes (we will call that \textit{lumped}) or the mean state differentiated with respect to some vertex metric, say its degree (we will call them \textit{spectral} quantities).

The microscopic dynamics of a single node and its neighborhood splits up into two different mechanisms. \1 The \textit{autonomous} dynamics, that is the system evolving when left to its own without any interference, and \2 the \textit{enforced} dynamics, that are imposed on the node at a given time step. The autonomous part in turn falls into two categories: The \textit{spontaneous} transitions of a node, independent of its neighborhood, and the \textit{induced} transitions, such as an infection event, that need at least pair-wise treatment of the nodes.

In the model, not only local space is modeled as discrete, by a graph, but also time takes the shape of \textit{steps of equal length}. The updating mechanism of the state of each node in the network is however done in a random order, thereby minimizing artifacts arising from the size of the time step, at least when looking at ensemble averages.

Furthermore, the system is fully \textit{Markovian} that is the systems future depends only on the present state, not on the trajectory through state space it took to get there. For discrete time models this can be put as causality going from one time step to the very next only. However, when it comes to the \textit{evolving} models, this property is only conserved because the immune record is also considered \textit{state}. So it is of no importance in which order, or when exactly, a node got infected with a certain virus strain, only the presence of the antigenic marker in the node's immune memory matters.

\begin{figure}[!ht]
  \begin{tikzpicture}[x=.05\linewidth,y=.05\linewidth]

      \node [anchor=north west] (UML) at (1, 1){
        \includegraphics[width=\linewidth] {
          plots/model/uml_small2.pdf
        }
      };

  \end{tikzpicture}
  \caption{An excerpt of the model's \gls{uml} class diagram. Colors highlight the most central classes. A more detailed version is appended (\ref{fig:uml-full})} \label{fig:uml-small}
\end{figure}

The model is split up into a number of classes, an overview being depicted in figure \ref{fig:uml-small}. In the following, we will explain the model's workings by looking at only the four most central classes: \1 \texttt{Autonoumous} controls how the epidemic unfolds in absence of forcings. \2 \texttt{Forcing} manages enforced state transitions in a generic manner. \3 The \texttt{Evolving} class provides the effective evolution of a pathogen in antigenic shape space. And \4 \texttt{Network} delivers information about the network being used.

One main idea of the \texttt{EpidNet} model is to put the most widely used compartmental models, such as $SI$, $SIS$, $SIR$, $SIRS$, $SEIR$, $SEIRD$, $SIRV$, \textit{\&c.} into a general framework. The model names usually codify the following compartments:
%
\begin{itemize}
  \item[]$S$---\textit{susceptible}: The ground state of being vulnerable to infection with a disease
  \item[]$E$---\textit{exposed}: The state of having contracted the disease, but not being infectious yet, with constant probability $\varepsilon$ to go to \textit{infectious}
  \item[]$I$---\textit{infectious}: The only \textit{active} state of the system, that pushes neighbors into \textit{exposed} or \textit{infectious} with a probability $\beta$, and itself transitions with constant rate $\gamma$ into \textit{resistant}, and $\delta$ into \textit{deceased}
  \item[]$R$---\textit{resistant}: The state after being infectious, that is not susceptible (yet) due to immunity (waning with constant rate $\rho$)
  \item[]$D$---\textit{deceased}: The state that is reached when an infection is fatal (\gls{ifr}: $\delta/\gamma$)
  \item[]$V$---\textit{vaccinated}: The same function as \textit{resistant}, but with a different transition rate $\sigma$ to susceptible.
\end{itemize}
%
Further compartments that were not included are for example \textit{latent} (infectious but unnoticed) or \textit{maternally immune} (resistant from maternal antibodies).

The model names usually codify the existence of compartments and their \textit{rough} ordering in the life cycle of an infection. For instance an appended $S$ denotes the \textit{perpetual} nature of the model: There is a finite probability to regain susceptibility and the cycle can be iterated several times. This has two immediate consequences: \1 the fraction of \textit{susceptible} individuals in the model does not necessarily decay \textit{monotonously} any more, and \2, the final fraction of infectious individuals $i_\infty$ (the attractor) can be larger than $0$.


\section{The \texttt{Autonomous} class} \label{sec:model-auton}



The \texttt{Autonomous} class treats each node in the system based on its current health state and the state of its vicinity. It is initialized at model construction and remains constant throughout the simulation.

Dependent on the choice whether the model is $SI$, $SIS$, $SIR$, ... up to $SEIRDVS$, the parameters of the corresponding transitions need to be provided. This is best illustrated in diagram \ref{fig:trans} which is in itself a directed graph, with nodes being health states, and arcs transitions. We may rephrase it that a node \textit{decays} from one ``species'' into another by the constant single-time step probabilities given by the Greek letter denoted at each arc.

\begin{figure}
  \begin{tikzpicture}

    \node [anchor=north west] (imgA) at (.05\linewidth, .050\linewidth){
      \includegraphics[width=.8\textwidth] {
        plots/model/Transitions.pdf
      }
    };

  \end{tikzpicture}
  \caption{The \texttt{Autonomous} transition diagram between all six possible compartments, with annotated single-time step probabilities. Dotted arrows are alternative wirings if the same-color target compartment is omitted. Except for the yellow $\beta$ lines, all transitions are autonomous. Observe that this automaton is itself a (directed) graph, which is exploited in its matrix representation \ref{eqn:transMat}} \label{fig:trans}
\end{figure}


We will in the following use the notation that $S,~ E,~ I,~R,~D,~\text{and}~V$ denote the \textit{compartments} present in the model, while $|S\rangle,~ |E\rangle,~ |I\rangle,~ |R\rangle,~ |D\rangle,~ \text{and}~|V\rangle$ are \textit{states} of individual vertices. So diagram \ref{fig:trans} translates into the following spontaneous reaction equations
%
\begin{align}\label{eqn:reacSpont}
  &\quad |E\rangle\stackrel{\varepsilon}\longrightarrow |I\rangle \nonumber\\
  &\begin{cases}
    |I\rangle\stackrel{\gamma}\longrightarrow |R\rangle,\quad\exists R \nonumber\\
    |I\rangle\stackrel{\gamma}\longrightarrow |S\rangle,\, \quad\nexists R \nonumber\\
  \end{cases}\\
  &\quad |I\rangle\stackrel{\delta}\longrightarrow |D\rangle\\
  &\quad |R\rangle\stackrel{\rho}\longrightarrow |S\rangle \nonumber\\
  &\quad |V\rangle\stackrel{\sigma}\longrightarrow |S\rangle \nonumber,
\end{align}
%
while the \textit{induced} transitions can be formulated pairwise
\begin{align}\label{eqn:reacIndu}
  &\begin{cases}
    &|S\rangle ~ \otimes ~ |I\rangle \quad\stackrel{1 -(1-\beta)^w}\longrightarrow\quad |E\rangle ~ \otimes ~ |I\rangle,\quad\exists E\\
    &|S\rangle ~ \otimes ~ |I\rangle \quad\stackrel{1 -(1-\beta)^w}\longrightarrow\quad |I\rangle~ ~ \otimes ~ |I\rangle,\quad\nexists E
  \end{cases}
\end{align}

where $w$ denotes the weight of the edge between the nodes.

\textit{Parallel} edges, say with weights $w_e$ and $w_f$, that both connect the vertices $i$ and $j$, can be treated as a single edge of weight $w_e + w_f$, as can be seen by looking at the counter probability of the transmission along the edge(s):
\begin{align*}
  (1-\beta)^{w_e} (1-\beta)^{w_f} = (1-\beta)^{w_e + w_f}
\end{align*}

So the model allows for either summing the weights that are parallel, or simply treating each parallel edge independently with no intricacies arising other than the representation as an adjacency matrix is now ambiguous. The adjacency matrix $\mathbf{A}$is therefore replaced by the weight matrix $\mathbf{W}$
\begin{align*}
  (\mathbf{W})_{ij} = w_{ij} \coloneqq
  \begin{cases}
    \sum_e w_{e},\quad &\forall e = (i, j)\\
    0,&\text{otherwise}
  \end{cases}
\end{align*}

If we now define a vector representation for each state, that is
\begin{align*}
  |S\rangle \doteq\begin{pmatrix}1 \\0 \\0 \\0 \\0 \\0 \\\end{pmatrix}, 
  \qquad|E\rangle \doteq\begin{pmatrix}0 \\1 \\0 \\0 \\0 \\0 \\\end{pmatrix},~~ ...
\end{align*}

the \textit{spontaneous} transitions can be formulated by means of multiplication with a \textit{transition matrix} $\mathbf{T}$ as in
\begin{align}
  |\psi \rangle_\mathrm{out} \doteq
  \begin{pmatrix}
    \psi_{S} \\
    \psi_{E} \\
    \psi_{I} \\
    \psi_{R} \\
    \psi_{D} \\
    \psi_{V} \\
  \end{pmatrix}_\mathrm{out}
  =
  ~~\begin{pmatrix}
    0 & 0 & \gamma_{\nexists R} & \rho & 0 & \sigma \\
    0 & 1-\varepsilon & 0 & 0 & 0 & 0 \\
    0 & \varepsilon & 1-\gamma-\delta & 0 & 0 & 0 \\
    0 & 0 & \gamma_{\exists R} & 1-\rho & 0 & 0 \\
    0 & 0 & \delta
     & 0 & 1 & 0 \\
    0 & 0 & 0 & 0 & 0 & 1-\sigma \\
  \end{pmatrix}
  *
  \begin{pmatrix}
    X_{S} \\
    X_{E} \\
    X_{I} \\
    X_{R} \\
    X_{D} \\
    X_{V} \\
  \end{pmatrix}_\mathrm{in}\label{eqn:transMat}
\end{align}

where $|\psi\rangle_\mathrm{out}$ is a not a final health state, but instead contains the probabilities such that $\sum_{X\in \{S, E, ...\}} \psi_X = 1$, and the rates form the entries of a weight matrix corresponding to figure \ref{fig:trans}.

Each column has to sum to 1, and $\gamma \in \{ \gamma_{\exists R}, \gamma_{\nexists R}\}$

With this, we can write the full-blown $S(E)I(R)DVS$ transition equation as
\begin{align}
  |\psi_i\rangle_\text{out}
  = \Bigg(&\sum_{j=1}^{N}\Big(|IS\rangle\langle IS|~(1-\beta)^{w_{ji}}~|X_j\rangle\otimes \nonumber \\
  &\qquad+ |IE\rangle\langle IS|~\big(1-(1-\beta_{\exists E})^{w_{ji}}\big)~|X_j\rangle\otimes \nonumber \\
  &\qquad+ |II\rangle\langle IS|~\big(1-(1-\beta_{\nexists E})^{w_{ji}}\big)~|X_j\rangle\otimes\Big) \nonumber \\
  ~&+~ (1-\varepsilon)|E\rangle\langle E| ~+~ \varepsilon|I\rangle\langle E|\\
  ~&+~ (1-\gamma-\delta)|I\rangle\langle I| ~+~ \gamma_{\exists R}|R\rangle\langle I|
  ~+~ \gamma_{\nexists R}|S\rangle\langle I| ~+~ \delta|D\rangle\langle I| \nonumber \\
  ~&+~ (1-\rho)|R\rangle\langle R| ~+~ \rho|S\rangle\langle R| \nonumber \\
  ~&+~ (1-\sigma)|V\rangle\langle V| ~+~ \sigma|S\rangle\langle V|\quad\Bigg)\quad |X_i\rangle_\text{in}~, \nonumber 
\end{align}
a long equation, but for instance due to 
\begin{align}
  (1-\beta)^{w_{ij}} = 1,\quad \forall w_{ij} = 0, 
\end{align}
the sum over all vertices $j = 1\ ...\ N$ is really just over the vicinity $\mathcal{V}(i)$.

Note that the out state is not a final state, but contains the probabilities to be in the respective compartment. Therefore, we let it collapse
\begin{align}
    |X\rangle_\text{out} \stackrel{\text{coll.}}\longleftarrow|\psi\rangle_\text{out}
\end{align}
by simply drawing a state at random weighted by the corresponding probability $\psi_X$.

Denote that this is \textit{full} complexity, and for less complex models $SI$, $SIR$, ... many terms can be omitted as the associated rates are taken as zero.

Summing up, we observe that a single node with its health state \textit{together} with this \texttt{Autonomous} transition operator \textit{and} the weight matrix fulfills the conditions of a \textit{stochastic finite-state automaton}:

\begin{enumerate}
  \item Its set of states is finite, of cardinality between $2$ ($SI$) and $6$ ($SEIRDV$).
  \item Its input alphabet is finite:\\
  $\{\text{``Perform a time step.''},~\text{``Your neighbor is $|S\rangle$.''}, ..., \text{``Your neighbor is $|V\rangle$.''}\}$
  \item It has an initial state, we chose it be ``\textit{susceptible}''.
  \item The state transition function is $T_{XY}$, it returns probabilities for every state.
  \item Its set of final states contains all states whose diagonal element of $T_{XY}$ is $1$, for example $|D\rangle$.
\end{enumerate}

Note that the $i$th column vector of the weight matrix $w_{ji}$ must be taken as part of the information of the automaton (part of $T$), as its entries are in $\mathbb{R}$ and not from a \textit{finite} alphabet. The automaton will then when in state $|S\rangle$ and receiving ``Perform a time step.'' iterate over its vicinity. The output of the machine is twofold: whether it did transition, and whether the reached state is final.

In transition diagram \ref{fig:trans}, there is no in-edge to the vaccinated state. That is because in this model, the system does not vaccinate autonomously, without intervention.Transitions to the vaccinated state can only happen by means of a second class \texttt{Forcing}


\section{The \texttt{Forcing} class} \label{sec:model-forcing}

By means of the \texttt{Forcing} class, the model allows for the specification of an arbitrary number of enforced transitions from one state to another, at any time step. All that needs to be provided is the set of \textit{source} compartments and the target compartment, as well as the time and a rate, which we will call $\omega$. This is best illustrated by the following configuration listing (\texttt{YAML language}):
\begin{lstlisting}[language=yaml]
forcings:
  Initially Infectious:
    source: S
    target: I
    time: 0
    rate: .01
\end{lstlisting}
This forcing is active at time step zero only, and makes one percent---selected at random out of the $N$ nodes---pass from susceptible to infectious.

The number of forcings applied over the entire simulation, or at one time step, is not limited, each forcing can be added as a dictionary of arbitrary name. When an (ordered) pair of time values is specified, the forcing's active time is from the first entry to the last time step \textit{before} the second entry. The rate $\omega$ can also be specified as an ordered pair, where the rate $\omega$ applied at each time step is linearly interpolated between the first and the last active time step.

Furthermore, there exists the possibility to specify a \textit{criterion} by which the nodes will be ordered before the forcing is applied. This can be any of the vertex metrics introduced in subsection \ref{sssec:graphmetr}. Giving another example, the listing below would first perform a vaccination of the highest degree nodes, and subsequently vaccinate notes at random with an increasing rate.

\begin{lstlisting}[ language=yaml,
                    caption={Example Specification of Forcings (\texttt{YAML} language). The full configuration file is appended (\ref{lst:fullcfg}).},
                    captionpos=b,
                    label={lst:forc2}]
forcings:
  First Vaccination Campaign:
    source: S
    target: V
    time: [100, 200]
    rate: [.0001]
    criterion: degree
    order: descending
  Vaccination Speeding Up:
    source: [S, R]
    target: V
    time: [200, 250]
    rate: [.0001, .0005]
    criterion: random
  ...
\end{lstlisting}



\section{The \texttt{Evolving} class} \label{sec:model-evo}

So far, we have considered compartmental models where immunity is accounted for by two compartments: resistant $R$ and vaccinated $V$. Each of these compartments has their own ``decay rate'' $\rho$, and $\sigma$, respectively. This implies that the immunity times are exponentially distributed with expectation $\nicefrac{1}{\rho}$, and $\nicefrac{1}{\sigma}$---\textit{independent of the epidemic situation in the system}. In fact, what drives the loss of immunity for many diseases, is \textit{not} the mere passing of time, but instead an immune escape undertaken by the pathogen. The resistance against the strain of the \textit{infection/vaccination} can remain intact over longer timescales, while the strains \textit{circulating} in the population change their characteristics eventually leading to some of them not being recognized by the immune system any more. At this point, the pathogen can re-infect hosts despite their immunity to older strains.

\begin{figure}
  \begin{tikzpicture}[x=.05\linewidth,y=.05\linewidth]
    \tikzstyle{every node}=[rounded corners=5pt, line width=3pt, inner sep=10pt]

    \node[fill=yellow!0, draw=white, anchor=south east] (a) at (-1, 4) {(a)};

    \node[fill=yellow!20, draw=yellow!40] (S1) at (0, 4) {\Large $S$};
    \node[fill=red!20, draw=red!40] (I) at (4, 4) {\Large $I$};
    \node[fill=green!20, draw=green!40] (R) at (8, 4) {\Large $R$};
    \node[fill=yellow!20, draw=yellow!40] (S2) at (12, 4) {\Large $S$};

    \node[fill=white!20] (dots) at (16, 4) {\Large ...};

    \node[fill=yellow!0, draw=white] (becomes) at (6, 2) {becomes};

    \node[fill=yellow!0, draw=white, anchor=south east] (b) at (-1, 0) {(b)};

    \node[fill=yellow!20, draw=yellow!40] (S) at (0, 0) {\Large $S$};
    \node[fill=red!20, draw=red!40] (I star) at (4, 0) {\Large $I$};
    \node[fill=yellow!20, draw=yellow!40] (S star) at (8, 0) {\Large $S$*};

    \node[fill=white!20] (dots star) at (12, 0) {\Large ...};

    \path[->, line width=2pt, draw=gray] (S1) -- (I);
    \path[->, line width=2pt, draw=gray] (I) -- (R);
    \path[->, line width=2pt, draw=gray] (R) -- (S2);
    \path[->, line width=2pt, draw=gray] (S2) -- (dots);

    \path[->, line width=2pt, draw=gray] (S) -- (I star);
    \path[->, line width=2pt, draw=gray] (I star) -- (S star);
    \path[->, line width=2pt, draw=gray] (S star) -- (dots star);

    % \draw (0, 0) node {\Large S} -- (4, 0) node {\Large I}; % -- (8, 0) node {\Large R} -- (12, 0) {\Large S} -- (16, 0) node[opacity=.0] {\Large ...};
  \end{tikzpicture}
  \caption{The flow diagram of infection (a) modeled by the \texttt{Autonomous} class \textit{solely}, versus (b) the immune functionality added by the \texttt{Evolving} class. The star hints at the presence of the immune marker.} \label{fig:sis*}
\end{figure}

In this section, we will extend the model to account for this pathway of immunity loss. It will be a most simple mechanistic approach with the aim to let the immune escape probability increase with the number of people infected, while at the same time produce a bursty nature of outbreak patterns. In simple terms, we ``replace'' the $R$ compartment by a \textit{modified} $S$, depicted in flow diagram \ref{fig:sis*}.

Building upon the existence of an antigenic shape space \citep{smithLapedes+04}, we introduce a position vector $\mathbf{a}$ in this $d$-dimensional space. Every infection is then associated with its antigenic position $\mathbf{a}$. After recovery, the position vector is added to the node's immune memory, a set we call $\mathcal{A}$. So the reaction equations \ref{eqn:reacSpont} and \ref{eqn:reacIndu} now need to account for the immune memory as an additional state, and the \textit{spontaneous} transitions become
%
\begin{align}
  &\quad |E,\ \mathbf{a},\ \mathcal{A}\rangle\stackrel{\varepsilon}\longrightarrow |I,\ \mathbf{a},\ \mathcal{A}\rangle \nonumber\\
%
  &\begin{cases}
    |I,\ \mathbf{a},\ \mathcal{A}\rangle
    \stackrel{\gamma}\longrightarrow
    |R,\ \mathcal{A} \cup \{\mathbf{a}\}\rangle,
    \quad\exists R \nonumber\\
%
    |I,\ \mathbf{a},\ \mathcal{A}\rangle
    \stackrel{\gamma}\longrightarrow
    |S,\ \mathcal{A} \cup \{\mathbf{a}\}\rangle,
    \, \quad\nexists R \nonumber\\
  \end{cases}\\
%
  &\quad |I,\ \mathbf{a},\ \mathcal{A}\rangle
  \stackrel{\delta}\longrightarrow |D,\ \mathcal{A}\rangle\\
%
  &\quad |R,\ \mathcal{A}\rangle
  \stackrel{\rho}\longrightarrow |S,\ \mathcal{A}\rangle \nonumber\\
%
  &\quad |V,\ \mathcal{A}\rangle
  \stackrel{\sigma}\longrightarrow
  |S,\ \mathcal{A}\rangle. \nonumber
\end{align}
%
Note that the state is not out of a finite set any more, as both $\mathbf{a}$ and $\mathcal{A}$ are unbounded. For the \textit{induced} transitions, we now write
%
\begin{align}
  \begin{cases}
    \quad|S,\ \mathcal{A}\rangle_i \otimes|I,\ \mathbf{a}_j,\ \mathcal{A}\rangle
    \quad\stackrel{\beta,\ \mathbf{a}_j\text\notim~ \mathcal{A}_i}\longrightarrow
    \quad|E,\ \mathbf{a}_j,\ \mathcal{A}\rangle_i
    \otimes
    |I,\ \mathbf{a}_j,\ \mathcal{A}\rangle,
    \quad\exists E\\
%
    \quad|S,\ \mathcal{A}\rangle_i \otimes|I,\ \mathbf{a}_j,\ \mathcal{A}\rangle
    \quad\stackrel{\beta,\ \mathbf{a}_j\text\notim~ \mathcal{A}_i}\longrightarrow
    \quad|I,\ \mathbf{a}_j,\ \mathcal{A}\rangle_i
    \otimes
    |I,\ \mathbf{a}_j,\ \mathcal{A}\rangle,
    \quad~\nexists E \label{eqn:evo-trans}
  \end{cases}
\end{align}
%
Here, $\mathbf{a}_j\text\notim~ \mathcal{A}_i$ denotes that the antigenic position vector $\mathbf{a}_j$ is not \textit{recognized} from the set $\mathcal{A}_i$, depending on its similarity to the elements of $\mathcal{A}$. This will be discussed below. Also, denote that node $i$ gets infected with antigen $\mathbf{a}_j$.

The antigenic vector is initialized as $\mathbf{a}(t=0) = \mathbf{0}$ and evolves according to
%
\begin{align} \label{eqn:evoStep}
  &|E,\ \mathbf{a}(t),\ \mathcal{A}\rangle ~\longrightarrow~|E,\ \mathbf{a}(t) + \mathbf{G}(\mathbf{0}, \nu),\ \mathcal{A}\rangle\\
  &|I,\ \mathbf{a}(t),\ \mathcal{A}\rangle ~\longrightarrow~|I,\ \mathbf{a}(t) + \mathbf{G}(\mathbf{0}, \nu),\ \mathcal{A}\rangle \nonumber,
\end{align}
%
where $\mathbf{G}(\mathbf{\mu},\nu)$ is a vector drawn from $d$ Gaussian distributions centered around $\mu_i$ with standard deviation $\nu$, the \textit{evolutionary speed}. This shape has twofold motivation. \1 It represents to some extent the mechanics of evolution: the most likely outcome is the predecessor, and variations are distributed symmetrically around that in either antigenic direction. \2 The expected position from the initial position $\mathbf{0}$ is distributed as a $d$-dimensional Gaussian \textit{even at early times}.

In addition, we take antigenic coordinates to be \textit{whole} numbers, that is $\mathbf{a}\in\mathbb{Z}^d$. This shall reflect the fact that evolution when modeled via genetic material, also takes discrete values. 

\subsubsection*{Immune recognition}

Whenever a node gets infected, that is it becomes \textit{exposed} or \textit{infectious}, it inherits the antigenic position, let us call it $\mathbf{a}_0$, from the transmitter, puts a copy into its immune memory $\mathcal{A}$, and then lets $\mathbf{a}_0$ evolve according to \ref{eqn:evoStep}. After it has recovered and is susceptible to infection again, it retains immunity at first, and then becomes increasingly susceptible, the further away the antigenic position of a potential infectious neighbor from its immune memory. \cite{pease87} found a linear increase in susceptibility over time, displayed in figure \ref{fig:pease}. In its most simple, logistic form, the susceptibility to reinfection by $\mathbf{a}$ under presence of immune marker $\mathbf{a}$ can then be written as
%
\begin{align}
  s_\text{contin}(\mathbf{a}, \mathbf{a}_0, \Theta)~&=~ \frac{\|\mathbf{a}-\mathbf{a}_0\|}{\sqrt{\|\mathbf{a}-\mathbf{a}_0\|^2+\Theta^2}}, \label{eqn:scontin}
\end{align}
%
with some constant $\Theta$. Then, the expected susceptibility at time $t$ of a single antigen vector performing a Gaussian random walk in $\mathbb{R}^d$ starting at $\mathbf{a}_0 = 0$ can be expressed as
%
\begin{align}
  \mathrm{E}[s_\text{contin}](t) &= \int_{\mathbb R^d} \dif ^{\ d} \! a~~s(\mathbf{a}, \mathbf{0}, \Theta)~~\frac{e^{-\frac{\|\mathbf{a}\|^2}{2t\nu^2}}}{\sqrt{2\pi t\nu^2}^d}\\
   &= \frac{1}{\sqrt{2\pi t\nu^2}^d}\int_{\mathbb R^d} \dif ^{\ d} \! a~~\frac{a}{\sqrt{a^2+\Theta^2}}~~e^{-\frac{a^2}{2t\nu^2}},
\end{align}
%
integrating over all possible places for $\mathrm{a}$, and taking $a \coloneqq \|\mathbf{a}\|$. This does not trivially decompose into tractable integrals, due to the root in the denominator. We leave this form of $s_\text{contin}(\mathbf{a}, \mathbf{a}_0, \Theta)$ for \textit{numerical} investigation, and simplify the computation, by declaring $\Theta$ the \textit{threshold}, below which susceptibility is null, and above which it is $1$. This \textit{discontinuous} function gives a more useful ``kernel'' for the integral
%
\begin{align}
  \mathrm{E}[s_\text{disc}](t) &= \frac{1}{\sqrt{2\pi t\nu^2}^d}\int_{\mathbb R^d} \dif ^{\ d} \! a~~\chi_{[\Theta,\ \infty]}(a)~~e^{-\frac{a^2}{2t\nu^2}}\\
  &= \frac{1}{\sqrt{2\pi t\nu^2}^d}\int_{\mathbb R^{d-1}}\dif\Omega\int_{\Theta}^\infty \dif a~~a^{d-1}~~e^{-\frac{a^2}{2t\nu^2}}, \label{eqn:sdisc}
\end{align}
%
where we assumed a \textit{Euclidean} metric $\|\cdot\|_2$ to directly split into spherical integration and integration over the radius $a$, with the angular parts of the Jacobian as part of $\dif\Omega$. Our goal is to derive the \textit{characteristic time scale} of this process. To obtain it, we have to perform the integral. The lack of angular dependence allows us to simplify $\int\dif\Omega = 2\pi^{d/2}/\Gamma(d/2) \eqqcolon \Omega_d$, for $d$ dimensions and the gamma function $\Gamma$:
%
\begin{align}
  \mathrm{E}[s_\text{disc}](t) =~~~& \frac{\Omega_d}{\sqrt{2\pi\nu^2}^d}\int_{\Theta}^\infty \dif a~~a^{d-1}~~e^{-\frac{a^2}{2t\nu^2}}\qquad\qquad\Big| u \coloneqq \frac{1}{2t\nu^2}\\
  =~~~& u^d\Omega_d\pi^{-d/2}\int_{\Theta}^\infty \dif a~~a^{d-1}~~e^{-ua^2}\\
  \stackrel{d~\text{even}}=~& u^d\Omega_d\pi^{-d/2}\int_{\Theta}^\infty \dif a~~(-\partial_u)^{\frac{d}{2}-1}(-\frac{\partial_a}{2u})~~e^{-ua^2}.
\end{align}
%
In the last step, we excluded \textit{odd} dimensions, as they make integration without the use of error functions impossible. For even dimensions we obtain
%
\begin{align}
  \mathrm{E}[s_\text{disc}](t)
  \stackrel{d~\text{even}}=~& -\frac{\Omega_d}{2}\left( \frac{u}{\pi}\right) ^{d/2}~(-\partial_u)^{\frac{d}{2}-1}~\frac{\int_{\Theta}^\infty \dif a~(\partial_a)~e^{-ua^2}}{u}\\
  =~~~& +\frac{\Omega_d}{2}\left( \frac{u}{\pi}\right) ^{d/2}~(-\partial_u)^{\frac{d}{2}-1}~\frac{e^{-u\Theta^2}}{u}\\
  =~~~& ~\sum_{i=0}^{d/2-1}\frac{\left(u\Theta^2\right)^{i}}{i!}~~e^{-u\Theta^2},\label{eqn:expsusc}
\end{align}
%
which for $d=2$ can be set equal to a desired probability and solved for $t$:
\begin{align}
  \mathrm{E}[s_\text{disc}](t)
  \stackrel{d=2}=~&~e^{-u\Theta^2} ~\stackrel{!}=~1-\frac{1}{e}\\
  \Leftrightarrow\qquad\qquad
  \tau^\text{EVO} \coloneqq ~~&t(\mathrm{E}[s]=\frac{e-1}{e}) 
  ~~= ~~\frac{\Theta^2}{2\nu^2(1-\log(e-1))} \label{eqn:tauEvo}
\end{align}
%
This gives us a measure for the timescale of the evolutionary process. Already for $d=4$, the analytic continuation of the product log must be employed for a solution, and higher dimensions, are completely intractable by the $t$ dependent terms in the polynomial \textit{and} the exponential.

Only heuristically can be observed that the expected step size (the radius) grows with the square root of $d$, as can be calculated either from the integral over $\mathbf{G}(\mathbf{0},\nu)$, or from the sum of $d$ independent steps of expected size $\nu$:
%
\begin{align}
  \langle \left(\| \mathbf a\|_2\right)^2\rangle
  &= \langle a_1a_1 + a_2a_2 + ... + a_da_d\rangle\\
  &= \langle a_1\rangle\langle a_1\rangle+\langle a_2\rangle\langle a_2\rangle+\langle ... \rangle+\langle a_d\rangle\langle a_d\rangle\\
  &= d \cdot \nu^2.
\end{align}
%
So roughly, the timescale can be thought of as shrinking linearly with $d$, when replacing $\nu^2$ in equation \ref{eqn:tauEvo} by $d\nu^2$.

%
\begin{figure}
  \begin{tikzpicture}[x=.05\linewidth,y=.05\linewidth]
    \node [anchor=north west] (a) at (1, 1) {(a)};
    \node [anchor=north west] (box) at (1, 1){
      \includegraphics[width=.5\linewidth] {
        plots/model/kernelBox.pdf
      }
    };
    \node [anchor=north west] (b) at (11, 1) {(b)};
    \node [anchor=north west] (notch) at (11, 1){
      \includegraphics[width=.5\linewidth] {
        plots/model/kernelNotch.pdf
      }
    };
  \end{tikzpicture}
  \caption{The susceptibility as a function of the radius (for the $\|\cdot\|_2$ norm), and two different shapes: (a) $\Theta$-like jump, and (b) initially linear increase sloping off as in \ref{eqn:scontin}. The red and orange vertical lines show the single-time step evolution ``speed'' $\sigma$, and the threshold $\Theta$, respectively.} \label{fig:kernel}
\end{figure}

\section{The \texttt{Network} class} \label{sec:model-nw}

The \texttt{EpidNet} model allocates an own class to the network, and delegates all sub-functionality regarding the network to it. It enables the study of \textit{multilayer} structures, where each layer is created independently and can be switched on/off at desired times. The idea is to enable minimal dynamic functionality: The networks per se are modeled static, but the \texttt{Layer} class allows for specification of \textit{activation} and \textit{deactivation} times. This can best be seen in \texttt{YAML} listing \ref{lst:netw}.
%
\begin{lstlisting}[ language=yaml,
                    caption={Example Network Specification (\texttt{YAML} language). The full configuration file is appended (\ref{lst:fullcfg}).},
                    captionpos=b,
                    label={lst:netw}]
network:
  layers:
    Friendships:
      activation_times: [0, 200]
      deactivation_times: [100]
      deactivation_factor: 0.0
      weight: 1.
      create_graph:
        model: load_from_file
        load_from_file:
          filename: adolescent_health_comm40.gv
          format: gv
          network_full_name: AddHealth Comm. 40
    School Activity:
      weight: .1
      mapping: random
      create_graph:
        model: KlemmEguiluz
        num_vertices: 2587
        mean_degree: 40
        KlemmEguiluz:
          mu: .5
\end{lstlisting}
%
The number of layers is unlimited in theory.
