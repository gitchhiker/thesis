% !TEX root = 0-THESIS.tex


\chapter{Discussion} \label{chp:disc}

In this thesis I undertook two main steps with regard to epidemic modeling: \1 I put the wide spectrum of compartmental models into one single matrix formulation forming the core of the network based model \texttt{EpidNet} with wide adaptability. The model's numerical results were tested against mass-action and \glsfirst{dbmf} theory. \2 I added an effective evolutionary mechanism to the model and explored the system's dependence on its control parameters: the \textit{basic reproductive number} $\Ro$ and the separation between epidemic and evolutionary time scale $\T$.

As to the first part, the numerical simulations with \texttt{EpidNet} could qualitatively reproduce the \gls{dbmf} estimator for the epidemic threshold to a sufficient degree. For the $SIR$ model there was good agreement between the onset of epidemics in simulation and the predicted value from \gls{dbmf}, except for the \glsfirst{ws} model, which showed a numerical threshold about twice as high as the one predicted. The simulation results also showed that \gls{ode} based estimators deviate for scale-free networks with offsets being the biggest for low-exponent networks. Only the high-density \glsfirst{ps} network was in good agreement, which is the expected tendency, and underlines the applicability of well-mixed approaches.

For the $SIS$ model, the comparison between numerics, and the \gls{dbmf} and \gls{ode} approaches revealed a similar relation: For all but the \gls{ws} and \gls{ps} networks, the \gls{ode} approach overestimated the threshold observed in numerics. The degree-based estimator could with good accuracy ameliorate this, agreeing with all simulated results, except the \PS. It seems that in this case, the degree-based epidemic threshold estimator is not an all-rounder that renders \gls{ode} approaches unnecessary. Rather, numerics, degree-based theory, and the mass-action description should be seen as complementing each other.

The generally good agreement of EpidNet's simulation results with the well-known \gls{ode} and \gls{dbmf} approaches can be seen as a successful sanity check for the model's approach and implementation. The generally better agreement with the more sophisticated DBMF (as compared to ODE) adds to this.

The \gls{ode} estimate of the final prevalence $i_\infty$ showed to be a good approximation for generated networks in the high $\Ro$ regime. Even for the scale-free \glsfirst{ba} and \glsfirst{ke} networks, agreement was fair, while for all four real-world networks there were considerable discrepancies. The fact that the generated networks share this behavior despite their dissimilar construction mechanisms is staggering. That all real-world networks deviate by a remarkably larger piece can be seen as a manifestation of the diversity of real-world networks. They contain additional complexity (like communities, or weighted edges) that the generated networks in our selection do not reproduce.


With the evolving $SIS*$ model, we attempted to model immunity loss by a more sophisticated mechanism than a decay of constant probability back into the susceptible compartment. The immunity escape of a pathogen by a random walk in an effective shape space was able to produce endemic and disease-free final states, with a chaotic transient in between. The evolutionary pressure on the pathogen by a step-function like threshold produced clusters in antigenic space for the transient region.

I analyzed the dependency of the final infectious fraction on the separation of time scales $\T$ and on the reproductive number $\Ro$, and found the following: Epidemic constellations of high $\Ro$ allowed pathogens to persist at slower evolutionary speed, and at a higher prevalence levels. The observed threshold-like rise was most likely a remnant of the used step-function of susceptibility together with the Gaussian random walk and the recovery times being exponentially distributed. Whenever the pathogen managed to escape its region of initial confinement in antigenic space, the vastness of the 2-$d$ plane offered enough antigenic diversity to permeate the network at a high-prevalence level.

Many observations we made can be attributed to modeling simplifications rather than the real-world behavior of evolving pathogens. For instance the featurelessness of antigenic space put the pathogen in no trade-off situation. Once the antigenically diverse endemic state was reached, the evolutionary pressure on the pathogen was negligibly small. Only for the \gls{ws} networks where clustering is exceptionally high, a remaining dependence of the final state on evolutionary speed could be observed. Another strong assumption is the dimensionality, which does not only quantitatively change the time scale, but also qualitatively decide whether for example self-confinement can happen with a finite probability or not.

One feature of \texttt{EpidNet}'s $SIS*$ model which cannot be matched with real-world observations is that the endemic equilibrium seems to be always of higher prevalence than the initial outbreak. This is clearly in contradiction with diseases being able to persist over centuries at a low number of cases after an initial epidemic or pandemic.

Another finding is that for certain, constructed network topologies, such as \gls{ws}, there are instances where a lower network connectivity \textit{can} enable more slowly evolving pathogens to survive. This holds for the narrow window of evolutionary speeds that are not high enough to allow the pathogen to survive in a well-mixed system, but not too low, such that it can generate an escaping mutation over the passage of a loop in the network. This effect could not be observed for scale-free or random networks.

In conclusion, it may be added that the \texttt{EpidNet} model is best seen as exploratory for examining interesting networks or spreading processes upon them. To that end, its functionality includes more than what can be analyzed and used in one thesis.

In conclusion, this work provides a promising though not field-ready approach for including evolutionary effects for immune escape. Even more importantly, the \texttt{EpidNet} model developed in the present work successfully simulates epidemics on generated and real-world networks to a level reproducing well-known theoretical results. It therefore provides a flexible and operable basis for the development of domain-specific models for research including but not limited to epidemiological studies.







